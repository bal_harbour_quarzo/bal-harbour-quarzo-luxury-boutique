Come experience a unique hotel destination designed for luxury and tranquility in Miami and Bal Harbour the playground of South Florida. Please contact our hotels reservation team to reserve a spacious vacation suite with designer amenities, ocean views and warm hospitality in Miami Beach. Send us your information and well help you design the Miami vacation you imagine . . . with luxurious suites overlooking the ocean, spa rituals, chef dining and warm Florida sunshine.

Address: 290 Bal Bay Drive, Bal Harbour, FL 33154

Phone: 877-233-7662
